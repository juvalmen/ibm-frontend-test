export interface ExpenseHistorial {
  id: number;
  idCard: number;
  idClient: number;
  expenseDate: Date;
  expenseDateString: String;
  expenseDesc: string;
  amount: number;
  amountString: string;
  clientName: string;
  address: string;
  phoneNumber: string;
  idCity: number;
  cityName: string;
  cardDesc: string;
  ccv: string;
  cardType: string;
}
