export interface Card {
  id:number;
	name: string,
	ccv: string,
	idTypeCard:number
	valueTypeCard:string
  descriptionTypeCard?:string
}