export interface Advisor{
  id:number;
	name: string,
	idTypeAdvisor:number
	valueTypeAdvisor:string
  descriptionTypeAdvisor?:string
}