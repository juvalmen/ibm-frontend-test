import { Injectable } from '@angular/core';
import { Constants } from '../../app/utils/constants';
import { HttpClient, HttpParams } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { BaseService } from '../base/base.service';

@Injectable({
  providedIn: 'root'
})
export class TypeService extends BaseService {
  constructor(public http: HttpClient) { 
    super(http);
  }

  findTypesByKey(key:String){
      let params = new HttpParams().append("key",key.toString());
      return this.http.get(Constants.GET_TYPE_KEY_SERVICE,{params})
          .pipe(map(data => {
              return data;
          }),
      catchError(this.handleError<any>('findTypesByKey')));
  }
}
