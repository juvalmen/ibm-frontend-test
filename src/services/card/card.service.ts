import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map,catchError } from 'rxjs/operators';
import { Constants } from '../../app/utils/constants';
import { Card } from '../../interfaces/card.interface';
import { BaseService } from '../base/base.service';

@Injectable({
  providedIn: 'root'
})
export class CardService extends BaseService {

  constructor(public http:HttpClient) {
    super(http);
  }

  saveCard(card: Card) {
    console.log('Constants.CARD_SAVE', Constants.CARD_SAVE);
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this.http.post(Constants.CARD_SAVE,card,{headers: headers})
            .pipe(map(data => {
                return data;
            }),
          catchError(this.handleError<any>('saveCard')));
  }

  getCards(){
      return this.http.get(Constants.CARD_ALL)
          .pipe(map(data => {
              return data;
          }),
        catchError(this.handleError<any>('getCards')));
  }

  deleteCard(idCard:number){
    return this.http.delete(Constants.CARD_DELETE + "/" + idCard)
        .pipe(map(data => {
            return data;
        }),
     catchError(this.handleError<any>('deleteCard')));
  }

  updateCard(card:Card){
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this.http.put(Constants.CARD_UPDATE,card,{headers: headers})
            .pipe(map(data => {
                return data;
            }),
          catchError(this.handleError<any>('updateCard')));
  }


}
