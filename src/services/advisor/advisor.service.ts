import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map,catchError } from 'rxjs/operators';
import { Constants } from '../../app/utils/constants';
import { Advisor } from '../../interfaces/advisor.interface';
import { BaseService } from '../base/base.service';

@Injectable({
  providedIn: 'root'
})
export class AdvisorService extends BaseService {

  constructor(public http:HttpClient) {
    super(http);
  }

  saveAdvisor(advisor:Advisor){
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this.http.post(Constants.ADVISOR_SAVE,advisor,{headers: headers})
            .pipe(map(data => {
                return data;
            }),
          catchError(this.handleError<any>('saveAdvisor')));
  }

  getAdvisors(){
      return this.http.get(Constants.ADVISOR_ALL)
          .pipe(map(data => {
              return data;
          }),
        catchError(this.handleError<any>('getAdvisors')));
  }

  deleteAdvisor(idAdvisor:number){
    return this.http.delete(Constants.ADVISOR_DELETE + "/" + idAdvisor)
        .pipe(map(data => {
            return data;
        }),
     catchError(this.handleError<any>('deleteAdvisor')));
  }

  updateAdvisor(advisor:Advisor){
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this.http.put(Constants.ADVISOR_UPDATE,advisor,{headers: headers})
            .pipe(map(data => {
                return data;
            }),
          catchError(this.handleError<any>('updateAdvisor')));
  }


}
