import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map,catchError } from 'rxjs/operators';
import { Constants } from '../../app/utils/constants';
import { Client } from '../../interfaces/client.interface';
import { BaseService } from '../base/base.service';

@Injectable({
  providedIn: 'root'
})
export class ClientService extends BaseService {

  constructor(public http:HttpClient) {
    super(http);
  }

  saveClient(client: Client) {
    console.log(client);
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this.http.post(Constants.CLIENT_SAVE,client,{headers: headers})
            .pipe(map(data => {
                return data;
            }),
          catchError(this.handleError<any>('saveClient')));
  }

  getClients(){
      return this.http.get(Constants.CLIENT_ALL)
          .pipe(map(data => {
              return data;
          }),
        catchError(this.handleError<any>('getClients')));
  }

  deleteClient(idClient:number){
    return this.http.delete(Constants.CLIENT_DELETE + "/" + idClient)
        .pipe(map(data => {
            return data;
        }),
     catchError(this.handleError<any>('deleteClient')));
  }

  updateClient(client:Client){
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this.http.put(Constants.CLIENT_UPDATE,client,{headers: headers})
            .pipe(map(data => {
                return data;
            }),
          catchError(this.handleError<any>('updateClient')));
  }

  getClientsByFilter(clientString:string) {
    return this.http.get(Constants.CLIENT_ALL_BY_FILTER+'/'+clientString)
        .pipe(map(data => {
            return data;
        }),
      catchError(this.handleError<any>('getClientsByFilter')));
  } 


}
