import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { of } from 'rxjs/observable/of';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

private item:any;
  constructor(protected http:HttpClient) { }

  saveStorage(itemName:string,itemContent:any){
      //dispositivo
      localStorage.setItem(itemName,JSON.stringify(itemContent));
  }

  removeStorageItem(itemName:string){
      //pc
      localStorage.removeItem(itemName);
  }

  getStorageItem(itemName:string){
    let promesa = new Promise((resolve,reject) => {
        if(localStorage.getItem(itemName)){
          this.item = JSON.parse(localStorage.getItem(itemName));
        }
        resolve();
    });
    return promesa;
  }

  getItem(){
    return this.item;
  }

  protected handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error.error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(error.error as T);
    };
  }
}
