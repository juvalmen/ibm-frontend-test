import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map,catchError } from 'rxjs/operators';
import { Constants } from '../../app/utils/constants';
import { BaseService } from '../base/base.service';

@Injectable({
  providedIn: 'root'
})
export class CityService extends BaseService {

  constructor(public http:HttpClient) {
    super(http);
  }

  getCities(){
      return this.http.get(Constants.CITY_ALL)
          .pipe(map(data => {
              return data;
          }),
        catchError(this.handleError<any>('getCities')));
  }
  
  getCitiesByFilter(cityString:string) {
      return this.http.get(Constants.CITY_ALL_BY_FILTER+'/'+cityString)
          .pipe(map(data => {
              return data;
          }),
        catchError(this.handleError<any>('getCitiesByFilter')));
  }

}
