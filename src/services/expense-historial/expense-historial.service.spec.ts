import { TestBed } from '@angular/core/testing';

import { ExpenseHistorialService } from './expense-historial.service';

describe('ExpenseHistorialService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const expenseHistorial: ExpenseHistorialService = TestBed.get(ExpenseHistorialService);
    expect(expenseHistorial).toBeTruthy();
  });
});
