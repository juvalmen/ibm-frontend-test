import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map,catchError } from 'rxjs/operators';
import { Constants } from '../../app/utils/constants';
import { BaseService } from '../base/base.service';

@Injectable({
  providedIn: 'root'
})
export class ExpenseHistorialService extends BaseService {

  constructor(public http:HttpClient) {
    super(http);
  }
  
  getAllExpenseByClient(idClient:number) {
    return this.http.get(Constants.HISTORIAL_BY_CLIENT + '/'+idClient)
          .pipe(map(data => {
              return data;
          }),
        catchError(this.handleError<any>('getAllExpenseByClient')));
  }

}
