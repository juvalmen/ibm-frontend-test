export class Constants {
    public static get BACK_END_URL():string {return 'http://localhost:8080';};
    public static get FRONT_URL(): string { return this.BACK_END_URL+'/api'; };
    public static get JSON_MIME_TYPE(): string { return "application/json"; };
    public static get FORMAT_DATE(): string{ return 'yyMMddHHmmss.sss' };
    
    // Client
    public static get CLIENT_ALL():string{return this.FRONT_URL+'/client/getAll'};
    public static get CLIENT_SAVE():string{return this.FRONT_URL+'/client/save'};
    public static get CLIENT_DELETE():string{return this.FRONT_URL+'/client/delete'};
    public static get CLIENT_UPDATE(): string{ return this.FRONT_URL + '/client/update' }
    public static get CLIENT_ALL_BY_FILTER(): string{ return this.FRONT_URL + '/client/getAllByFilter' };
    // Advisor
    public static get ADVISOR_ALL():string{return this.FRONT_URL+'/advisor/getAll'};
    public static get ADVISOR_SAVE():string{return this.FRONT_URL+'/advisor/save'};
    public static get ADVISOR_DELETE():string{return this.FRONT_URL+'/advisor/delete'};
    public static get ADVISOR_UPDATE(): string{ return this.FRONT_URL + '/advisor/update' }
    // Card
    public static get CARD_ALL():string{return this.FRONT_URL+'/card/getAll'};
    public static get CARD_SAVE():string{return this.FRONT_URL+'/card/save'};
    public static get CARD_DELETE():string{return this.FRONT_URL+'/card/delete'};
    public static get CARD_UPDATE(): string{ return this.FRONT_URL + '/card/update' }
    
    // City
    public static get CITY_ALL():string{return this.FRONT_URL+'/city/getAll'};
    public static get CITY_ALL_BY_FILTER(): string{ return this.FRONT_URL + '/city/getAllByFilter' };
    
    // Historial
    public static get HISTORIAL_BY_CLIENT():string{return this.FRONT_URL+'/expense/getAllExpenseByClient'};

    public static get GET_TYPE_SERVICE():string { return this.FRONT_URL+'/type/getAll' };
    public static get GET_TYPE_KEY_SERVICE():string { return this.FRONT_URL+'/type/getByKey' };
    public static get TYPE_ADVISOR_KEY(): string{return 'ASESOR_TIPO'};
    public static get TYPE_CARD_KEY(): string{return 'TARJETA_TIPO'};

    public static get MANGA_TYPE():number { return 1 };
    public static get NUM_LINES():number { return 8};
    public static get SYSTEM_ERROR():string{return "Error del sistema"};
    public static get STATUS():any[]{return [
            {label:'Seleccione Estado', value:null},
            {label:'Activo', value:1},
            {label:'Inactivo', value:0}
        ]};

  }
