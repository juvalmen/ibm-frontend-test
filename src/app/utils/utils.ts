import { Injectable } from '@angular/core';

@Injectable()
export class Utils {

  constructor() {
  }

  static getCurrenDateString(fecha?: Date) { 

    let currentDate;
    if (fecha != null) {
      currentDate = new Date(fecha);
    } else { 
      currentDate = new Date();
    }      

    let currentYear = currentDate.getUTCFullYear();
    let currentMonth = currentDate.getUTCMonth()+1;
    let currentDay = currentDate.getUTCDate();

    let currentMonthString;
    let currentDayString;

    if (currentMonth < 10) {
      currentMonthString = "0" + currentMonth.toString();
    } else { 
      currentMonthString = currentMonth.toString();
    }
    if (currentDay < 10) {
      currentDayString = "0" + currentDay.toString();
    } else { 
      currentDayString = currentDay.toString();
    }

    let fechaActual = currentYear + "-" + currentMonthString + "-" + currentDayString;
    
    return fechaActual;
  }

}
