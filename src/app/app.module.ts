import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2PaginationModule } from 'ng2-pagination';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HomeComponent } from './home/home.component';
import { MenubarModule } from 'primeng/menubar';
import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button';
import { GalleriaModule } from 'primeng/galleria';
import { InputMaskModule } from 'primeng/inputmask';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { InputTextModule } from 'primeng/inputtext';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { CardModule } from 'primeng/card';
import { SpinnerModule } from 'primeng/spinner';
import { PickListModule } from 'primeng/picklist';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { AccordionModule } from 'primeng/accordion';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { DialogModule } from 'primeng/dialog';
import { ToastModule} from 'primeng/toast';
import { MenuComponent } from './menu/menu.component';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ClientAdminComponent } from './client-admin/client-admin.component';
import { AdvisorAdminComponent } from './advisor-admin/advisor-admin.component';
import { CardAdminComponent } from './card-admin/card-admin.component';
import { ExpenseHistorialAdminComponent } from './expense-historial-admin/expense-historial-admin.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ClientAdminComponent,
    AdvisorAdminComponent,
    CardAdminComponent,
    ExpenseHistorialAdminComponent,
    MenuComponent,
  ],
  imports: [
    BreadcrumbModule,
    CalendarModule,
    DropdownModule,
    MessagesModule,
    MessageModule,
    InputTextModule,
    FieldsetModule,
    TableModule,
    MenubarModule,
    GalleriaModule,
    ButtonModule,
    PanelModule,
    AccordionModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    Ng2PaginationModule,
    CardModule,
    SpinnerModule,
    PickListModule,
    InputMaskModule,
    ConfirmDialogModule,
    DynamicDialogModule,
    DialogModule,
    ToastModule,
    AutoCompleteModule
  ],
  providers: [
    {
      provide: LocationStrategy, useClass: HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
