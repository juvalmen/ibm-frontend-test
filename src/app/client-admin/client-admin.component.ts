import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { BaseService } from '../../services/base/base.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import { first } from 'rxjs/operators';

import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Client } from 'src/interfaces/client.interface';
import { TypeService,ClientService, CityService } from 'src/services/service.index';
import { City } from 'src/interfaces/city.interface';
import { InputMask } from 'primeng/inputmask';


@Component({
  selector: 'app-client-admin',
  templateUrl: './client-admin.component.html',
  styleUrls: ['./client-admin.component.css'],
  providers: [MessageService,ConfirmationService]
})
export class ClientAdminComponent extends BaseComponent implements OnInit {
  @ViewChild("myInputMask") myInputMask: InputMask;

  hiddenCreate:boolean;
  formClient: FormGroup;
  clients: Client[] = [];
  cities: City[] = [];
  cols: any[];
  attr: any;
  //Autocomplete
  cityObject: City;  
  keyword: string;

  constructor(private message:MessageService,
              protected base:BaseService,
              private _fb: FormBuilder,
              private typeService:TypeService,
              private clientService: ClientService,
              private cityService: CityService,
              private confirmationService: ConfirmationService) {
    super(message,base);

    this.initForm();
    this.initComponents();
   }

   initForm(){
    this.formClient = this._fb.group({
      "id":[null],
      "name":[null, Validators.required],
      "address":[null],
      "phoneNumber":[null],     
      "city":[null, Validators.required],     
    });
   }

  ngOnInit() {
    this.items = [
            {label:'Cliente',icon: 'pi pi-fw pi-user-plus'},
    ];
    this.home = {icon: 'pi pi-home',routerLink: ['/home']};
    this.cols = [
        { field: 'name', header: 'Nombre' },
        { field: 'address', header: 'Dirección' },
        { field: 'phoneNumber', header: 'Teléfono' },
        { field: 'cityName', header: 'Ciudad' }
    ];
    this.getClients();
  }

  initComponents(){
    this.hiddenCreate = true;
    this.attr = { attributeValue: "" };
  }

  search(event) {
    this.cityService.getCitiesByFilter(this.keyword.toUpperCase()).pipe(first())
    .subscribe(
        data => {
          if (data != undefined) {
            if(data.responseCode == "200"){
               this.cities = data.responseBody;
            } else {
              this.addMessage('Error',data.responseMessage,'error');            }
          } else {
            this.addMessage('Error','Ocurrio algo con su solicitud','error');
          }
        },
        error => {
          this.addMessage('Error',error,'error');
        });
  }

  getClients(){
    this.clientService.getClients().pipe(first())
    .subscribe(
        data => {
          if (data != undefined) {
            if(data.responseCode == "200"){
               this.clients = data.responseBody;
            } else {
              this.addMessage('Error',data.responseMessage,'error');            }
          } else {
            this.addMessage('Error','Ocurrio algo con su solicitud','error');
          }
        },
        error => {
          this.addMessage('Error',error,'error');
        });
  }

  saveOrUpdateClient(){
      const client :Client = {
       id :this.formClient.value.id,
       name:this.formClient.value.name,
       address:this.formClient.value.address,
       phoneNumber :this.formClient.value.phoneNumber,
       idCity :this.formClient.value.city.id,
      }
    console.log('client', client);
      if(client.id == null){
        this.saveClient(client);
      }
      else{
        this.updateClient(client);
      }
   
  }

  saveClient(client:Client){
    this.clientService.saveClient(client).pipe(first())
    .subscribe(
        data => {
          if (data != undefined && data != null) {
            if(data.responseCode == "200"){
              this.addMessage('',data.responseMessage,'info');
              this.getClients();
            } else {
              this.addMessage('',data.responseMessage,'warn');
            }
          } else {
            this.addMessage('','Ocurrio algo con su solicitud','error');           
           }
        },
        error => {
          this.addMessage('','Ocurrio algo con su solicitud' + error,'error');
    });
  }

  updateClient(client:Client){
    this.clientService.updateClient(client).pipe(first())
    .subscribe(
        data => {
          if (data != undefined && data != null) {
            if(data.responseCode == "200"){
              this.addMessage('',data.responseMessage,'info');
              this.getClients();
            } else {
              this.addMessage('',data.responseMessage,'warn');
            }
          } else {
            this.addMessage('','Ocurrio algo con su solicitud','error');           
           }
        },
        error => {
          this.addMessage('','Ocurrio algo con su solicitud' + error,'error');
    });
  }

  isInputMaskFilledPhone(event) {
    if (!this.myInputMask.filled) {
      this.attr = { attributeValue: "" };
    }
  }

  createClient(){
    this.initComponents();
    this.hiddenCreate = false;
    this.formClient.patchValue({
       
    });
  }

  cancelClient(){
    this.initComponents();
    this.formClient.patchValue({
       
    });
  }

  deleteConfirm(client: Client) {
    this.confirmationService.confirm({
        message: ' Estas seguro de eliminar el cliente ?',
        accept: () => {
            this.deleteClient(client);
        }
    });
  }

  deleteClient(client: Client){
      this.clientService.deleteClient(client.id).pipe(first())
      .subscribe(
          data => {
            if (data != undefined && data != null) {
              if(data.responseCode == "200"){
                this.addMessage('',data.responseMessage,'info');
                this.getClients();
              } else {
                this.addMessage('',data.responseMessage,'warn');
              }
            } else {
              this.addMessage('','Ocurrio algo con su solicitud','error');           
             }
          },
          error => {
            this.addMessage('','Ocurrio algo con su solicitud' + error,'error');
      });
  }

  selectClient(client: Client){
    this.hiddenCreate = false;

    const city:City = {
     id:client.idCity,
     name:client.cityName
    }

    this.formClient.patchValue({
       id: client.id,
       name:client.name,
       address:client.address,
       phoneNumber:client.phoneNumber,      
       city:city      
    });
  }

}
