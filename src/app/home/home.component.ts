import { Component, OnInit, ViewChild } from '@angular/core';
import {MenuComponent} from '../menu/menu.component';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

@ViewChild(MenuComponent) menu: MenuComponent;

  private items: MenuItem[];
  home:MenuItem;
  constructor() { }

  ngOnInit() {
    this.items = [];
    this.home = {icon: 'pi pi-home',routerLink: ['/home']};
  }

}
