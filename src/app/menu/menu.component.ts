import { Component, OnInit, Input } from "@angular/core";
import { MenuItem } from "primeng/api";
import { Router } from "@angular/router";
import { Message } from "primeng/components/common/api";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.css"]
})
export class MenuComponent implements OnInit {
  items: MenuItem[];

  msgs: Message[] = [];
  @Input()
  message: string;

  constructor(private router: Router) {}

  showSuccess() {
    this.msgs = [];
    this.msgs.push({
      severity: "success",
      summary: this.message,
      detail: "Order submitted"
    });
  }

  showInfo() {
    this.msgs = [];
    this.msgs.push({
      severity: "info",
      summary: this.message,
      detail: "PrimeNG rocks"
    });
  }

  showWarn() {
    this.msgs = [];
    this.msgs.push({
      severity: "warn",
      summary: this.message,
      detail: "There are unsaved changes"
    });
  }

  showError() {
    this.msgs = [];
    this.msgs.push({
      severity: "error",
      summary: this.message,
      detail: "Validation failed"
    });
  }

  ngOnInit() {
    this.items = [
      {
        label: "Cliente",
        items: [
          {
            label: "Administración de clientes",
            routerLink: ["/client-admin"]
          }
        ]
      },
      {
        label: "Asesor",
        items: [
          {
            label: "Administración de asesores",
            routerLink: ["/advisor-admin"]
          }
        ]
      },
      {
        label: "Tarjeta",
        items: [
          {
            label: "Administración de tarjetas",
            routerLink: ["/card-admin"]
          }
        ]
      }
      ,
      {
        label: "Historial",
        items: [
          {
            label: "Historial por cliente",
            routerLink: ["/expense-historial-admin"]
          }
        ]
      }
    ];
  }
}
