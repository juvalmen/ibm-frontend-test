import { Component, OnInit, ViewChild } from "@angular/core";
import { Constants } from "../utils/constants";
import { BaseComponent } from "../base/base.component";
import { BaseService } from "../../services/base/base.service";
import { MessageService, ConfirmationService } from "primeng/api";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { first } from "rxjs/operators";

import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Type } from "src/interfaces/type.interface";
import { Card } from "src/interfaces/card.interface";
import { TypeService, CardService } from "src/services/service.index";
import { InputMask } from "primeng/inputmask";

@Component({
  selector: "app-card-admin",
  templateUrl: "./card-admin.component.html",
  styleUrls: ["./card-admin.component.css"],
  providers: [MessageService, ConfirmationService]
})
export class CardAdminComponent extends BaseComponent implements OnInit {
  @ViewChild("myInputMask") myInputMask: InputMask;
  @ViewChild("myInputMaskCcv") myInputMaskCcv: InputMask;

  hiddenCreate: boolean;
  formCard: FormGroup;
  cards: Card[] = [];
  cols: any[];
  attrCard: any;
  attrCcv: any;
  typeCards: Type[] = [];

  constructor(
    private message: MessageService,
    protected base: BaseService,
    private _fb: FormBuilder,
    private typeService: TypeService,
    private cardService: CardService,
    private confirmationService: ConfirmationService
  ) {
    super(message, base);

    this.initForm();
    this.initComponents();
  }

  initForm() {
    this.formCard = this._fb.group({
      id: [null],
      name: [null, Validators.required],
      ccv: [null, Validators.required],
      typeCard: [null, Validators.required]
    });
  }

  ngOnInit() {
    this.items = [{ label: "Tarjeta", icon: "pi pi-fw pi-user-plus" }];
    this.home = { icon: "pi pi-home", routerLink: ["/home"] };
    this.cols = [
      { field: "name", header: "Número" },
      { field: "ccv", header: "CCV" },
      { field: "descriptionTypeCard", header: "Tipo" }
    ];
    this.findTypesByKey();
    this.getCards();
  }

  initComponents() {
    this.hiddenCreate = true;
    this.attrCard = { attributeValue: "" };
    this.attrCcv = { attributeValue: "" };
  }

  findTypesByKey() {
    this.typeService
      .findTypesByKey(Constants.TYPE_CARD_KEY)
      .pipe(first())
      .subscribe(
        data => {
          if (data != undefined) {
            if (data.responseCode == "200") {
              this.typeCards = data.responseBody;
            } else {
              this.addMessage("Error", data.responseMessage, "error");
            }
          } else {
            this.addMessage("Error", "Ocurrio algo con su solicitud", "error");
          }
        },
        error => {
          this.addMessage("Error", error, "error");
        }
      );
  }

  getCards() {
    this.cardService
      .getCards()
      .pipe(first())
      .subscribe(
        data => {
          if (data != undefined) {
            if (data.responseCode == "200") {
              this.cards = data.responseBody;
            } else {
              this.addMessage("Error", data.responseMessage, "error");
            }
          } else {
            this.addMessage("Error", "Ocurrio algo con su solicitud", "error");
          }
        },
        error => {
          this.addMessage("Error", error, "error");
        }
      );
  }

  saveOrUpdateCard() {
    const card: Card = {
      id: this.formCard.value.id,
      name: this.formCard.value.name,
      ccv: this.formCard.value.ccv,
      idTypeCard: this.formCard.value.typeCard.idtype,
      valueTypeCard: this.formCard.value.typeCard.value
    };

    if (card.id == null) {
      this.saveCard(card);
    } else {
      this.updateCard(card);
    }
  }

  saveCard(card: Card) {
    this.cardService
      .saveCard(card)
      .pipe(first())
      .subscribe(
        data => {
          if (data != undefined && data != null) {
            if (data.responseCode == "200") {
              this.addMessage("", data.responseMessage, "info");
              this.getCards();
            } else {
              this.addMessage("", data.responseMessage, "warn");
            }
          } else {
            this.addMessage("", "Ocurrio algo con su solicitud", "error");
          }
        },
        error => {
          this.addMessage("", "Ocurrio algo con su solicitud" + error, "error");
        }
      );
  }

  updateCard(card: Card) {
    this.cardService
      .updateCard(card)
      .pipe(first())
      .subscribe(
        data => {
          if (data != undefined && data != null) {
            if (data.responseCode == "200") {
              this.addMessage("", data.responseMessage, "info");
              this.getCards();
            } else {
              this.addMessage("", data.responseMessage, "warn");
            }
          } else {
            this.addMessage("", "Ocurrio algo con su solicitud", "error");
          }
        },
        error => {
          this.addMessage("", "Ocurrio algo con su solicitud" + error, "error");
        }
      );
  }

  isInputMaskFilledCard(event) {
    if (!this.myInputMask.filled) {
      this.attrCard = { attributeValue: "" };
    }
  }

  isInputMaskFilledCcv(event) {
    if (!this.myInputMaskCcv.filled) {
      this.attrCcv = { attributeValue: "" };
    }
  }

  createCard() {
    this.initComponents();
    this.hiddenCreate = false;
    this.formCard.patchValue({
      typeCard: null
    });
  }

  cancelCard() {
    this.initComponents();
    this.formCard.patchValue({
      typeCard: null
    });
  }

  deleteConfirm(card: Card) {
    this.confirmationService.confirm({
      message: " Estas seguro de eliminar la tarjeta ?",
      accept: () => {
        this.deleteCard(card);
      }
    });
  }

  deleteCard(card: Card) {
    this.cardService
      .deleteCard(card.id)
      .pipe(first())
      .subscribe(
        data => {
          if (data != undefined && data != null) {
            if (data.responseCode == "200") {
              this.addMessage("", data.responseMessage, "info");
              this.getCards();
            } else {
              this.addMessage("", data.responseMessage, "warn");
            }
          } else {
            this.addMessage("", "Ocurrio algo con su solicitud", "error");
          }
        },
        error => {
          this.addMessage("", "Ocurrio algo con su solicitud" + error, "error");
        }
      );
  }

  selectCard(card: Card) {
    console.log(card);
    this.hiddenCreate = false;

    const type: Type = {
      idtype: card.idTypeCard,
      name: card.descriptionTypeCard,
      value: card.valueTypeCard
    };

    this.formCard.patchValue({
      id: card.id,
      name: card.name,
      ccv: card.ccv,
      typeCard: type
    });
  }
}
