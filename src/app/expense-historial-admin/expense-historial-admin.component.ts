import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { BaseService } from '../../services/base/base.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import { first } from 'rxjs/operators';

import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Client } from 'src/interfaces/client.interface';
import { ExpenseHistorialService, ClientService } from 'src/services/service.index';
import { InputMask } from 'primeng/inputmask';
import { ExpenseHistorial } from 'src/interfaces/expense-historial.interface';
import { Utils } from '../../app/utils/utils';
import { utils } from 'protractor';
import { CurrencyPipe, formatCurrency } from '@angular/common';


@Component({
  selector: 'app-expense-historial-admin',
  templateUrl: './expense-historial-admin.component.html',
  styleUrls: ['./expense-historial-admin.component.css'],
  providers: [MessageService,ConfirmationService]
})
export class ExpenseHistorialAdminComponent extends BaseComponent implements OnInit {
  @ViewChild("myInputMask") myInputMask: InputMask;

  hiddenCreate:boolean;
  formHistorial: FormGroup;
  clients: Client[] = [];
  historials: ExpenseHistorial[] = [];
  cols: any[];
  //Autocomplete
  idClient: number;  
  keyword: string;

  constructor(private message:MessageService,
              protected base:BaseService,
              private _fb: FormBuilder,
              private clientService: ClientService,
              private historialService: ExpenseHistorialService,
              private confirmationService: ConfirmationService) {
    super(message,base);

    this.initForm();
    this.initComponents();
   }

   initForm(){
    this.formHistorial = this._fb.group({
      "client":[null, Validators.required],     
    });
   }

  ngOnInit() {
    this.items = [
            {label:'Historial de consumo del cliente',icon: 'pi pi-fw pi-user-plus'},
    ];
    this.home = {icon: 'pi pi-home',routerLink: ['/home']};
    this.cols = [
        { field: 'cardDesc', header: 'Número de tarjeta' },
        { field: 'ccv', header: 'CCV' },
        { field: 'cardType', header: 'Tipo de tarjeta' },
        { field: 'expenseDesc', header: 'Descripción' },
        { field: 'expenseDateString', header: 'Fecha del consumo' },
        { field: 'amountString', header: 'Monto' }
    ];

  }

  initComponents(){
    this.hiddenCreate = true;
  }

  search(event) {
    this.clientService.getClientsByFilter(this.keyword.toUpperCase()).pipe(first())
    .subscribe(
        data => {
          if (data != undefined) {
            if(data.responseCode == "200"){
               this.clients = data.responseBody;
            } else {
              this.addMessage('Error',data.responseMessage,'error');            }
          } else {
            this.addMessage('Error','Ocurrio algo con su solicitud','error');
          }
        },
        error => {
          this.addMessage('Error',error,'error');
        });
  }

  onSelectAutoComplete(event) { 
    if (event != null && event != undefined) { 
      this.idClient = event.id;
    }      
  }
  
  onClearAutoComplete(event) { 
    this.idClient = null;
    this.historials = [];
   }

  searchHistorial(){
    if (this.idClient != null) { 
      this.historialService.getAllExpenseByClient(this.idClient).pipe(first())
      .subscribe(
        data => {
          if (data != undefined) {
            if(data.responseCode == "200"){
              this.historials = data.responseBody;
              this.formatValue();
            } else {
              this.addMessage('Error',data.responseMessage,'error');            }
          } else {
            this.addMessage('Error','Ocurrio algo con su solicitud','error');
          }
        },
        error => {
          this.addMessage('Error',error,'error');
        });
    }

  }

  formatValue() { 
    for (const iterator of this.historials) {
      iterator.expenseDateString = Utils.getCurrenDateString(iterator.expenseDate);
      iterator.amountString = formatCurrency(iterator.amount, 'en_US', '$');
    }  
  }
  
  limpiar(){ 
    this.historials = [];
  }

}
