import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpenseHistorialAdminComponent } from './expense-historial-admin.component';

describe('ExpenseHistorialAdminComponent', () => {
  let component: ExpenseHistorialAdminComponent;
  let fixture: ComponentFixture<ExpenseHistorialAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpenseHistorialAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpenseHistorialAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
