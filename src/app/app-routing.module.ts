import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ClientAdminComponent } from './client-admin/client-admin.component';
import { AdvisorAdminComponent } from './advisor-admin/advisor-admin.component';
import { CardAdminComponent } from './card-admin/card-admin.component';
import { ExpenseHistorialAdminComponent } from './expense-historial-admin/expense-historial-admin.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'client-admin',
    component: ClientAdminComponent
  },
  {
    path: 'advisor-admin',
    component: AdvisorAdminComponent
  },
  {
    path: 'card-admin',
    component: CardAdminComponent
  },
  {
    path: 'expense-historial-admin',
    component: ExpenseHistorialAdminComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
