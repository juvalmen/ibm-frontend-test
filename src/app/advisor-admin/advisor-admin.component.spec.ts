import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvisorAdminComponent } from './advisor-admin.component';

describe('AdvisorAdminComponent', () => {
  let component: AdvisorAdminComponent;
  let fixture: ComponentFixture<AdvisorAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvisorAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvisorAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
