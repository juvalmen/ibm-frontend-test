import { Component, OnInit } from '@angular/core';
import { Constants } from '../utils/constants';
import { BaseComponent } from '../base/base.component';
import { BaseService } from '../../services/base/base.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import { first } from 'rxjs/operators';

import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Type } from 'src/interfaces/type.interface';
import { Advisor } from 'src/interfaces/advisor.interface';
import { TypeService,AdvisorService } from 'src/services/service.index';

@Component({
  selector: 'app-advisor-admin',
  templateUrl: './advisor-admin.component.html',
  styleUrls: ['./advisor-admin.component.css'],
  providers: [MessageService,ConfirmationService]
})
export class AdvisorAdminComponent extends BaseComponent  implements OnInit {

  hiddenCreate:boolean;
  formAdvisor: FormGroup;
  advisors: Advisor[] = [];
  cols: any[];
  typeAdvisors: Type[] = [];

  constructor(private message: MessageService,
              protected base:BaseService,
              private _fb: FormBuilder,
              private typeService:TypeService,
              private advisorService: AdvisorService,
              private confirmationService: ConfirmationService) {
    super(message,base);

    this.initForm();
    this.initComponents();
   }

   initForm(){
    this.formAdvisor = this._fb.group({
      "id":[null],
      "name":[null, Validators.required],
      "typeAdvisor":[null, Validators.required]
    });
   }

  ngOnInit() {
    this.items = [
            {label:'Asesor',icon: 'pi pi-fw pi-user-plus'},
    ];
    this.home = {icon: 'pi pi-home',routerLink: ['/home']};
    this.cols = [
        { field: 'name', header: 'Nombre' },
        { field: 'descriptionTypeAdvisor', header: 'Tipo' }
    ];
    this.findTypesByKey();
    this.getAdvisors();
  }

  initComponents(){
    this.hiddenCreate = true;
  }

  findTypesByKey(){
    this.typeService.findTypesByKey(Constants.TYPE_ADVISOR_KEY).pipe(first())
    .subscribe(
        data => {
          if (data != undefined) {
            if(data.responseCode == "200"){
               this.typeAdvisors = data.responseBody;
            } else {
              this.addMessage('Error',data.responseMessage,'error');            
            }
          } else {
            this.addMessage('Error','Ocurrio algo con su solicitud','error');
          }
        },
        error => {
          this.addMessage('Error',error,'error');
        });
  }

  getAdvisors(){
    this.advisorService.getAdvisors().pipe(first())
    .subscribe(
        data => {
          if (data != undefined) {
            if(data.responseCode == "200"){
               this.advisors = data.responseBody;
            } else {
              this.addMessage('Error',data.responseMessage,'error');            }
          } else {
            this.addMessage('Error','Ocurrio algo con su solicitud','error');
          }
        },
        error => {
          this.addMessage('Error',error,'error');
        });
  }

  saveOrUpdateAdvisor(){
      const advisor :Advisor = {
        id :this.formAdvisor.value.id,
        name: this.formAdvisor.value.name,
        idTypeAdvisor:this.formAdvisor.value.typeAdvisor.idtype,
        valueTypeAdvisor :this.formAdvisor.value.typeAdvisor.value,
      }
      
      if(advisor.id == null){
        this.saveAdvisor(advisor);
      }
      else{
        this.updateAdvisor(advisor);
      }
   
  }

  saveAdvisor(advisor:Advisor){
    this.advisorService.saveAdvisor(advisor).pipe(first())
    .subscribe(
        data => {
          if (data != undefined && data != null) {
            if(data.responseCode == "200"){
              this.addMessage('',data.responseMessage,'info');
              this.getAdvisors();
            } else {
              this.addMessage('',data.responseMessage,'warn');
            }
          } else {
            this.addMessage('','Ocurrio algo con su solicitud','error');           
           }
        },
        error => {
          this.addMessage('','Ocurrio algo con su solicitud' + error,'error');
    });
  }


  updateAdvisor(advisor:Advisor){
    this.advisorService.updateAdvisor(advisor).pipe(first())
    .subscribe(
        data => {
          if (data != undefined && data != null) {
            if(data.responseCode == "200"){
              this.addMessage('',data.responseMessage,'info');
              this.getAdvisors();
            } else {
              this.addMessage('',data.responseMessage,'warn');
            }
          } else {
            this.addMessage('','Ocurrio algo con su solicitud','error');           
           }
        },
        error => {
          this.addMessage('','Ocurrio algo con su solicitud' + error,'error');
    });
  }


  createAdvisor(){
    this.initComponents();
    this.hiddenCreate = false;
    this.formAdvisor.patchValue({
      typeAdvisor: null, 
    });
  }

  cancelAdvisor(){
    this.initComponents();
    this.formAdvisor.patchValue({
      typeAdvisor: null, 
    });
  }


  deleteConfirm(advisor: Advisor) {
    this.confirmationService.confirm({
        message: ' Estas seguro de eliminar el asesor ?',
        accept: () => {
            this.deleteAdvisor(advisor);
        }
    });
  }

  deleteAdvisor(advisor: Advisor){
      this.advisorService.deleteAdvisor(advisor.id).pipe(first())
      .subscribe(
          data => {
            if (data != undefined && data != null) {
              if(data.responseCode == "200"){
                this.addMessage('',data.responseMessage,'info');
                this.getAdvisors();
              } else {
                this.addMessage('',data.responseMessage,'warn');
              }
            } else {
              this.addMessage('','Ocurrio algo con su solicitud','error');           
             }
          },
          error => {
            this.addMessage('','Ocurrio algo con su solicitud' + error,'error');
      });
  }

  selectAdvisor(advisor: Advisor){
    this.hiddenCreate = false;

    const type:Type = {
      idtype:advisor.idTypeAdvisor,
      name:advisor.descriptionTypeAdvisor,
      value:advisor.valueTypeAdvisor
     }

    this.formAdvisor.patchValue({
       id: advisor.id,
       name:advisor.name,
       typeAdvisor: type       
    });
  }

}
