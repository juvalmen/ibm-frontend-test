import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { FormBuilder, FormGroup} from '@angular/forms';
import { BaseService } from '../../services/base/base.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  providers: [MessageService]
})
export class BaseComponent implements OnInit {

  generalForm: FormGroup;
  protected sub: any;
  protected items: MenuItem[];
  home:MenuItem;
  msgs:string = '';

  loadingStatus = false;
  protected baseService: BaseService;
  
  constructor(public messageService: MessageService, protected base:BaseService) {
    
  }

  ngOnInit() {
  }

  // convenience getter for easy access to form fields
  get f() { return this.generalForm.controls; }

  addMessage(title:string,message:string,type:string) {
    this.clearMessage();
    this.messageService.add({severity:type, summary:title, detail:message});
  }

  clearMessage() {
    this.messageService.clear();
  }

}
